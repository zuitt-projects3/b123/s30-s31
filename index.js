const express = require("express")
const mongoose = require("mongoose")
const app = express();
const port = 4000;

// MongoDB connection

// change password to your DB password
// change myFirstDatabase to todoList123

mongoose.connect("mongodb+srv://princebarro:Arnaments123@cluster0.lw2dr.mongodb.net/todoList123?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true
  //
})
// notifications if connection to db is success or failed.
let db = mongoose.connection;
// console.error.bind(console,<message>) -print error in both terminal
// and browser
db.on("error",console.error.bind(console, "Connection Error."))
// if connection is succesful
db.once("open", ()=>console.log("Connected to MongoDB"))

// middleware - in expressjs context, are methods, functions and  adds features
// to your application.
// handles json data from our client.
app.use(express.json());

const taskRoutes = require('./routes/taskRoutes');
console.log(taskRoutes);
// A middleware to group all of our routes starting their endpoints
  // with /tasks
app.use('/tasks',taskRoutes)

// users

const userRoutes = require('./routes/userRoutes');
console.log(userRoutes)
app.use('/users', userRoutes)

app.listen(port, ()=>console.log(`Server is runnin at port ${port}`));
