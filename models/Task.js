const mongoose = require('mongoose');

// Mongoose Schema
  /*
    Schemas, in mongoose context, determine the structure of the documents. It
    acts as a blueprint to how data/entries in our collection should look like.
    Gone are the days when worry if entry has "stock" or "stocks" as field names,
    Mongoose itself will notify us and disallow us to create documents that do
    not mathc the schema.
  */
//Schema is a constructor from mongoose that will allow us to create a new
// Schema object.

/*
  Define the fields for the task document.
  This task document has a name and status field.
  name with the value to be a String.
  status has curly brace because it has defined fields to follow:
      value's type: String
      and if not provided a value it will
      default: "pending"
*/
const taskSchema = new mongoose.Schema({
  name: String,
  status: {
    type: String,
    default: "pending"
  }
});

// Model
/*
  Models are used to connect your API to the corresponding collection in your
  database. It is a representation of a task documents to be created into a
  new tasks collection.

  Models uses schemas to create/instantiate objects that corresponds to the
  schema. By default, when creating the collection from your model, the
  collection name will be pluralized (English Only)

  mongoose.model(<nameOfCollectionInAtlas>,<schemaToFollow>)

  Your models have methods that will help in querying, creating or even
  editing your documents.

  Models are also constructors of your documents.
*/
module.exports = mongoose.model("Task",taskSchema);
/*module.exports will allow us to export file/functions/values into another js
file within our application export the model to other files.*/
