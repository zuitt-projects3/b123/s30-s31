const express = require("express")
// allows us the access to HTTP method routes
// Router() will action as a middleware and our routing system.
const router = express.Router();
// Routes should only be concerned with our endpoints and methods.
// The action to be done once a route is accessed should be in a seprate file,
// It should be in our controllers
const taskControllers = require('../controllers/taskControllers');
const {createTasksController,
      getAllTasksController,
      getSingleItem,
      updateSingleTask,
      cancelSingleTask
} = taskControllers
router.post('/', createTasksController)
router.get('/', getAllTasksController)
router.get('/:id', getSingleItem)

router.put('/complete/:id', updateSingleTask)
router.put('/cancel/:id', cancelSingleTask)
// router holds all of our routes and can be exported and imported into another
// file
module.exports = router;
