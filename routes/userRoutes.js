// import express and Router
const express = require("express")
const router = express.Router();

const userControllers = require('../controllers/userControllers');

const {
  createUserController,
  getAllUserController,
  getSinglgeUserController,
  updateSingleUser
} = userControllers
router.post('/', createUserController)
router.get('/', getAllUserController)

//get single user route
router.get('/:id', getSinglgeUserController)
// update single user
router.put('/:id', updateSingleUser)
//export router which contains the Routes
module.exports = router;
