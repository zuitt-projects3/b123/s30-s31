const Task = require('../models/Task');

module.exports.createTasksController = (req,res) => {

    console.log(req.body);
    // Created a new tas object out of our task model.
    // newTask has added methods for use in our application



    // Model.findOne() is a mongoose method similar to findOne in MongoDB
    // However, with Model.findOne(), we can process the result via our API
    Task.findOne({name : req.body.name})
    /*
      .then() is able to capture the result of the query
      .then() is able to process the result and when that result is returned
      we can actually add another .then() to process the next result
    */

    // .catch() is able to capture the error of the query
    .then(result => {

    console.log(result);// returns null if there is no duplicate
    if(result !== null && result.name === req.body.name){
      return res.send("Duplicate Task Found")
    } else {
      // .save() is a method from an object created by a model.
      // This will then allow us to save our document into our collection
      // .save() can have an anonymous function and this can take 2 parameters
      // The first parameter: saveERR receives an error object if there is an
          // error in creating a document.
      // The second item is our saved document

       let newTask = new Task({
          name: req.body.name
        })
      newTask.save()
      .then(result => res.send(result))
      .catch(err => res.send(err))
    }
  })
    .catch(err => res.send(err));
}

module.exports.getAllTasksController = (req,res) => {

    Task.find({})
    .then(result => res.send(result))
    .catch(err => res.send(err));

  }

module.exports.getSingleItem = (req,res) => {
  console.log(req.params.id)
  Task.findById(req.params.id)
  .then(result => res.send(result))
  .catch(err => res.send(err));
}

module.exports.updateSingleTask = (req,res) => {
  console.log(req.params.id)

  let updates = {
    status: 'Complete'
  }
  Task.findById(req.params.id,updates,{new: true})
  .then(updatedStatus => res.send(updatedStatus))
  .catch(err => res.send(err))
}

module.exports.cancelSingleTask = (req,res) =>{
  console.log(req.params.id)
  let updates = {
    status: "Cancelled"

  }
  Task.findById(req.params.id,updates,{new: true})
  .then(canceledStatus => res.send(canceledStatus))
  .catch(err => res.send(err))
}
