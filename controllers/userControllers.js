// import user model
const User = require('../models/User')
module.exports.createUserController = (req,res) => {
    console.log(req.body);
     User.findOne({username: req.body.username},(err,result)=>{
       console.log(err);
       console.log(result);
       if(result !== null && result.username === req.body.username){
         return res.send("Duplicate Username Found!")
       } else {
         let newUser = new User ({
           username: req.body.username
         })
         newUser.save((saveErr,savedTask)=>{
           console.log(savedTask);
           if(saveErr){
             return console.error(saveErr);
           } else {
             return res.send('Successful Registration!')
           }
         })
       }
     })
  }
  module.exports.getAllUserController = (req,res) => {
    User.find({})
    .then(result => res.send(result))
    .catch(err => res.send(err));
  }

  module.exports.getSinglgeUserController = (req,res) => {
    // mongoose has a query called findById() which works like find({_id:"id"})
    console.log(req.params.id)
    User.findById(req.params.id)
    .then(result => res.send(result))
    .catch(err => res.send(err));
  }

  module.exports.updateSingleUser = (req,res) => {
    console.log(req.params.id)
    /*
      .findByIdAndUpdate() - looks an item by its id then add the update
      .findByIdAndUpdate(id,{updates},{new: true})

      UPDATES ARE ALWAYS AN OBJECT

      {new: true} - returns the updated document
    */

    // updates will contain the field and value we want to update
    let updates = {
      username: req.body.username
    }
    User.findByIdAndUpdate(req.params.id,updates,{new: true})
    .then(updatedUser => res.send(updatedUser))
    .catch(err => res.send(err));
  }
